A repository to allow translators to push their WebKitGTK+ translations
using Damned Lies, to be imported upstream by developers before a stable
release.
